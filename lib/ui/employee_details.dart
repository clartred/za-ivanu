import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:katedra_tfze/model/Employee.dart';
import 'package:katedra_tfze/repository/EmployeeRepository.dart';
import 'package:katedra_tfze/ui/all_employees.dart';

class EmployeeDetailsScreen extends StatelessWidget {
  Employee employee;

  EmployeeDetailsScreen({required this.employee});

  TextEditingController nameSurnameController = TextEditingController();
  TextEditingController titleController = TextEditingController();
  TextEditingController bioController = TextEditingController();
  TextEditingController emailController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    EmployeeRepository employeeRepo = EmployeeRepository(context: context);
    nameSurnameController.text = employee.name;
    titleController.text = employee.title;
    bioController.text = employee.bio;
    emailController.text = employee.email;
    return Scaffold(
      appBar: AppBar(
        leading: Image.network(
            'http://www.tfzr.uns.ac.rs/Templates/Frontend/images/grb_small.png'),
        title: Text('Izmeni Zaposlenog'),
      ),
      body: Container(
        child: Center(
            child: Column(
              children: [
                Card(
                  child: ListTile(
                      leading:  Text("Ime: " +  employee.name)
                 )
                ),
                Card(
                    child: ListTile(
                      leading: Text("Pozicija: " +  employee.title),
                    )
                ),
                Card(
                    child: ListTile(
                        leading:   Text("Opis: " +  employee.bio),
                    )
                ),
                Card(
                    child: ListTile(
                        leading:   Text("Email: " +  employee.email),
                    )
                ),
                ElevatedButton(
                    onPressed: () {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) => AllEmployeesScreen()));
                    },
                    child: Text('Idi Nazad'))
              ],
            )),
      ),
    );
  }
}
