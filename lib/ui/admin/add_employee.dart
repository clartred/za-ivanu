import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:katedra_tfze/repository/EmployeeRepository.dart';

class AddEmployeeScreen extends StatelessWidget {
  TextEditingController nameSurnameController = TextEditingController();
  TextEditingController titleController = TextEditingController();
  TextEditingController bioController = TextEditingController();
  TextEditingController emailController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    EmployeeRepository employeeRepo = EmployeeRepository(context: context);
    return Scaffold(
      appBar: AppBar(
        leading: Image.network(
            'http://www.tfzr.uns.ac.rs/Templates/Frontend/images/grb_small.png'),
        title: Text('Dodaj zaposlenog'),
      ),
      body: Container(
        child: Center(
            child: Column(
          children: [
            TextField(
              controller: nameSurnameController,
              keyboardType: TextInputType.text,
              decoration: InputDecoration(hintText: 'Ime i Prezime'),
            ),
            TextField(
              controller: titleController,
              keyboardType: TextInputType.text,
              decoration: InputDecoration(hintText: 'Pozicija'),
            ),
            TextField(
              controller: bioController,
              keyboardType: TextInputType.text,
              decoration: InputDecoration(hintText: 'Opis'),
            ),
            TextField(
              controller: emailController,
              keyboardType: TextInputType.text,
              decoration: InputDecoration(hintText: 'Email'),
            ),
            ElevatedButton(
                onPressed: () {
                  employeeRepo.addEmployee(nameSurnameController.text,
                      titleController.text, bioController.text, emailController.text);
                },
                child: Text('Dodaj'))
          ],
        )),
      ),
    );
  }
}
