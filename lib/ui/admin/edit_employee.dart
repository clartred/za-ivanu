import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:katedra_tfze/model/Employee.dart';
import 'package:katedra_tfze/repository/EmployeeRepository.dart';

class EditEmployeeScreen extends StatelessWidget {
  Employee employee;

  EditEmployeeScreen({required this.employee});

  TextEditingController nameSurnameController = TextEditingController();
  TextEditingController titleController = TextEditingController();
  TextEditingController bioController = TextEditingController();
  TextEditingController emailController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    EmployeeRepository employeeRepo = EmployeeRepository(context: context);
    nameSurnameController.text = employee.name;
    titleController.text = employee.title;
    bioController.text = employee.bio;
    emailController.text = employee.email;
    return Scaffold(
      appBar: AppBar(
        leading: Image.network(
            'http://www.tfzr.uns.ac.rs/Templates/Frontend/images/grb_small.png'),
        title: Text('Izmeni Zaposlenog'),
      ),
      body: Container(
        child: Center(
            child: Column(
          children: [
            TextField(
              controller: nameSurnameController,
              keyboardType: TextInputType.text,
              decoration: InputDecoration(hintText: 'Ime i Prezime', labelText: 'Ime i Prezime'),
            ),
            TextField(
              controller: titleController,
              keyboardType: TextInputType.text,
              decoration: InputDecoration(hintText: 'Pozicija', labelText: 'Pozicija'),
            ),
            TextField(
              controller: bioController,
              keyboardType: TextInputType.text,
              decoration: InputDecoration(hintText: 'Opis', labelText: 'Opis'),
            ),
            TextField(
              controller: emailController,
              keyboardType: TextInputType.text,
              decoration: InputDecoration(hintText: 'Email', labelText: 'Email'),
            ),
            ElevatedButton(
                onPressed: () {
                  employeeRepo.editEmployee(
                      nameSurnameController.text,
                      titleController.text,
                      bioController.text,
                      emailController.text,
                  employee.uid);
                },
                child: Text('Izmeni'))
          ],
        )),
      ),
    );
  }
}
