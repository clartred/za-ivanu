import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:katedra_tfze/ui/admin/add_employee.dart';
import 'package:settings_ui/settings_ui.dart';

class AdminSettingsScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: Image.network('http://www.tfzr.uns.ac.rs/Templates/Frontend/images/grb_small.png'),
        title: Text('Admin podešavanja'),
      ),
      body: SettingsList(
        sections: [
          SettingsSection(
            tiles: [
              SettingsTile(
                title: 'Dodaj zaposlenog',
                onPressed: (cont) {
                  Navigator.push(context, MaterialPageRoute(
                      builder: (context) => AddEmployeeScreen()));
                },
              ),
            ],
          )
        ],
      ),
    );
  }
}
