import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:katedra_tfze/model/Employee.dart';
import 'package:katedra_tfze/repository/EmployeeRepository.dart';
import 'package:katedra_tfze/ui/admin/admin_settings_screen.dart';
import 'package:katedra_tfze/ui/admin/edit_employee.dart';
import 'package:katedra_tfze/ui/login_screen.dart';
import 'package:katedra_tfze/ui/employee_details.dart';

class AllEmployeesScreen extends StatefulWidget {
  _AllEmployeesState createState() => _AllEmployeesState();
}

class _AllEmployeesState extends State<AllEmployeesScreen> {
  String filterKey = '';

  @override
  Widget build(BuildContext context) {
    EmployeeRepository employeeRepo = EmployeeRepository(context: context);
    bool isAdmin =
        FirebaseAuth.instance.currentUser!.uid == "PmTrreNzlPausLJnrBSR8tw8a523"
            ? true
            : false;
    return Scaffold(
      appBar: AppBar(
        leading: Image.network(
            'http://www.tfzr.uns.ac.rs/Templates/Frontend/images/grb_small.png'),
        title: Text("Kadar tfzr"),
        actions: [
          SizedBox(width: 24),
          GestureDetector(
            onTap: () {
              FirebaseAuth.instance.signOut();
              Navigator.pushReplacement(context,
                  MaterialPageRoute(builder: (context) => LoginScreen()));
            },
            child: Icon(Icons.logout),
          )
        ],
      ),
      floatingActionButton: isAdmin
          ? FloatingActionButton(
              child: Icon(Icons.settings),
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => AdminSettingsScreen()));
              },
            )
          : null,
      body: Container(
        child: Column(
          children: [
            ConstrainedBox(
              constraints: BoxConstraints(maxHeight: 60),
              child: FutureBuilder(
                  future: employeeRepo.getAllTitles(),
                  builder: (context, AsyncSnapshot<List<String>> snapshot) {
                    if (snapshot.hasData) {
                      return ListView.builder(
                          itemCount:
                              snapshot.hasData ? snapshot.data!.length : 0,
                          scrollDirection: Axis.horizontal,
                          itemBuilder: (_, index) {
                            return GestureDetector(
                              onTap: () {
                                setState(() {
                                  filterKey = snapshot.data!.elementAt(index);
                                });
                              },
                              child: Card(
                                child: Container(
                                    color: snapshot.data!.elementAt(index) ==
                                            filterKey
                                        ? Colors.blue
                                        : Colors.white,
                                    padding: EdgeInsets.only(left: 6, right: 6),
                                    child: Center(
                                        child: Text(
                                            snapshot.data!.elementAt(index)))),
                              ),
                            );
                          });
                    } else {
                      return Center(child: CircularProgressIndicator());
                    }
                  }),
            ),
            FutureBuilder(
                future: filterKey.isEmpty
                    ? employeeRepo.getAllEmployees()
                    : employeeRepo.getEmployeesByTitle(filterKey),
                builder: (context, AsyncSnapshot<List<Employee>> snapshot) {
                  print(snapshot);
                  if (snapshot.hasData) {
                    return ListView.builder(
                        itemCount: snapshot.hasData ? snapshot.data!.length : 0,
                        shrinkWrap: true,
                        itemBuilder: (_, index) {
                          return Card(
                            child: Container(
                                padding: EdgeInsets.only(top: 12, bottom: 12),

                                child: ListTile(
                                  leading: isAdmin
                                      ?  Column(
                                      children: [
                                      GestureDetector(
                                      onTap: () {
                                    Navigator.push(context, MaterialPageRoute(builder: (context) => EditEmployeeScreen(employee: snapshot.data!.elementAt(index)))); },
                                        child:(Icon(Icons.edit))),
                                        GestureDetector(
                                            onTap  : () { employeeRepo.deleteEmployee(snapshot.data!.elementAt(index).uid); },
                                            child:( Icon(Icons.delete))),
                                      ],
                                    )  : null,
                                   title: GestureDetector(
                                       onTap  : () {
                                         Navigator.push(context, MaterialPageRoute(builder: (context) => EmployeeDetailsScreen(employee: snapshot.data!.elementAt(index))));
                                         },
                                       child:Text( snapshot.data!.elementAt(index).name)
                                   ),

                                )// Text(
                                //
                                // ),

                                ),
                          );
                        });
                  } else
                    return Center(child: CircularProgressIndicator());
                }),
          ],
        ),
      ),
    );
  }
}
