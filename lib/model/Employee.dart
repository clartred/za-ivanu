class Employee {
  Employee.create(this.uid, this.name, this.title, this.bio, this.email);

  final String uid;
  final String name;
  final String title;
  final String bio;
  final String email;
}
