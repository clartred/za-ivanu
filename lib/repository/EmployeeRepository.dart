import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:katedra_tfze/model/Employee.dart';
import 'package:katedra_tfze/ui/all_employees.dart';

class EmployeeRepository {
  BuildContext context;

  EmployeeRepository({required this.context});

  final _databaseReference = FirebaseFirestore.instance;

  void addEmployee(name, title, bio, email) async {
    if (await doesTitleExist(title)) {
      pushEmployeeToDB(name, bio, title, email);
      pushTitleToDB(title);
    } else
      pushEmployeeToDB(name, bio, title, email);
  }

  void deleteEmployee(uid) async {
    try {
      _databaseReference.collection('employee').doc(uid).delete();
      //Navigator.pop(context);
      Navigator.push(context,
          MaterialPageRoute(builder: (context) => AllEmployeesScreen()));
    } catch (e) {}
  }

  void editEmployee(name, title, bio, email, uid) async {
    try {
      _databaseReference.collection('employee').doc(uid).set({
        'bio': bio,
        'email': email,
        'name': name,
        'title': title,
        'uid': uid
      });
      Navigator.pop(context);
    } catch (e) {}
  }

  void pushTitleToDB(title) {
    _databaseReference.collection('titles').add({'title': title});
  }

  void pushEmployeeToDB(name, bio, title, email) {
    try {
      _databaseReference
          .collection('employee')
          .add({'name': name, 'title': title, 'bio': bio, 'email': email}).then(
              (value) {
        _databaseReference
            .collection('employee')
            .doc(value.id)
            .update({'uid': value.id});
      });
      ScaffoldMessenger.of(context)
          .showSnackBar(SnackBar(content: Text('Uspešno dodavanje')));
      Navigator.push(context,
          MaterialPageRoute(builder: (context) => AllEmployeesScreen()));
    } catch (Exception) {
      ScaffoldMessenger.of(context)
          .showSnackBar(SnackBar(content: Text('Greška prilikom dodavanja')));
    }
  }

  Future<bool> doesTitleExist(title) async {
    QuerySnapshot qs = await _databaseReference
        .collection('titles')
        .where('title', isEqualTo: title)
        .get();
    if (qs.docs.isEmpty) {
      return true;
    } else
      return false;
    //!isEmpty ? false : true
  }

  Future<List<Employee>> getAllEmployees() async {
    QuerySnapshot qs = await _databaseReference.collection('employee').get();
    List<Employee> employees = [];
    qs.docs.forEach((element) {
      print("${element.data()} element");
      employees.add(Employee.create(element.get('uid'), element.get('name'),
          element.get('title'), element.get('bio'), element.get('email')));
    });
    return employees;
  }

  Future<List<String>> getAllTitles() async {
    QuerySnapshot qs = await _databaseReference.collection('titles').get();
    List<String> titles = [];
    qs.docs.forEach((element) {
      titles.add(element.get('title'));
    });
    return titles;
  }

  Future<List<Employee>> getEmployeesByTitle(title) async {
    QuerySnapshot qs = await _databaseReference
        .collection('employee')
        .where('title', isEqualTo: title)
        .get();
    List<Employee> employees = [];
    qs.docs.forEach((element) {
      employees.add(Employee.create(element.get('uid'), element.get('name'),
          element.get('title'), element.get('bio'), element.get('email')));
    });
    return employees;
  }
}
