import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:katedra_tfze/ui/all_employees.dart';

class UserRepository {
  final _databaseReference = FirebaseFirestore.instance;

  void loginUser(email, password, context) async {
    try {
      await FirebaseAuth.instance
          .signInWithEmailAndPassword(email: email, password: password);
      Navigator.pushReplacement(context,
          MaterialPageRoute(builder: (context) => AllEmployeesScreen()));
    } on FirebaseAuthException catch (error) {
      if (error.code == 'user-not-found') {
        final snackBar = SnackBar(content: Text('Korisnik nije pronadjen'));
        Scaffold.of(context).showSnackBar(snackBar);
      } else if (error.code == 'wrong-password') {
        final snackBar = SnackBar(content: Text('Pogrešna šifra'));
        Scaffold.of(context).showSnackBar(snackBar);
      }
    }
  }

  Future<void> createStudent(email, password, username, context) async {
    try {
      await FirebaseAuth.instance
          .createUserWithEmailAndPassword(email: email, password: password);
      addUserToDB(email, username, context);
    } on FirebaseAuthException catch (e) {
      if (e.code == 'weak-password') {}
    } catch (e) {
      print(e);
    }
  }

  void addUserToDB(email, username, context) async {
    try {
      await _databaseReference
          .collection('users')
          .doc(FirebaseAuth.instance.currentUser!.uid)
          .set({
        'uid': FirebaseAuth.instance.currentUser!.uid,
        'username': username,
        'email': email
      });
      Navigator.push(context,
          MaterialPageRoute(builder: (context) => AllEmployeesScreen()));
    } catch (e) {}
  }


}
